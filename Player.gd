extends Node2D

var dir : int = 0;
var speed : float = 200.0;


func _physics_process(delta: float) -> void:
	dir = 0;
	if(Input.is_action_pressed("player_left")):
		dir -= 1;
	if(Input.is_action_pressed("player_right")):
		dir += 1;
	
	self.position.x += dir * speed * delta;
